package com.poetry.nilsedgar.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
public class RatedPoem {
    
    public @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long ratedPoemId;
    public @CreationTimestamp @Temporal(TemporalType.TIMESTAMP) Date timeCreated;
    public @Enumerated(EnumType.STRING) RatingEnum rating;
    public @OneToOne @JoinColumn(name = "poem_id") Poem poem;


    public RatedPoem(){

    }

}
