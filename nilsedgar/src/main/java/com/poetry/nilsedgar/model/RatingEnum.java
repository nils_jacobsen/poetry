package com.poetry.nilsedgar.model;

public enum RatingEnum {
    READ, ENJOYED, LAUGHED, CRIED
}
