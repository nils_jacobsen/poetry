package com.poetry.nilsedgar.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
public class Poem {
    
    public @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long poemId;
    public @CreationTimestamp @Temporal(TemporalType.TIMESTAMP) Date timeCreated;
    public String title;
    public String text;
    public String notes;
    public String form;


    public Poem(){

    }

    public Poem(String title, String text){
        this.title = title;
        this.text = text;
        this.notes = null;
    }

    public Poem(String title, String text, String notes){
        this.title = title;
        this.text = text;
        this.notes = notes;
    }

}
