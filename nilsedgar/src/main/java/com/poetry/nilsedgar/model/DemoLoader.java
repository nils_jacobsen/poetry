package com.poetry.nilsedgar.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.poetry.nilsedgar.repository.PoemRepository;

@Component
public class DemoLoader implements CommandLineRunner {

    private final PoemRepository poemRepository;

    @Autowired
    public DemoLoader(PoemRepository poemRepository){
        this.poemRepository = poemRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        this.poemRepository.save(new Poem("My very first poem","This is a poem, it doesn't rhyme, hopefully I will, learn how to write, better poems", "just testing"));
        this.poemRepository.save(new Poem("two","testing test"));
        this.poemRepository.save(new Poem("three","hiiiiiiieee"));
    }
    
}
