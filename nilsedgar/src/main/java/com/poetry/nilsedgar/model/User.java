package com.poetry.nilsedgar.model;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
public class User {
    
    public @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long userId;
    public String userName;
    public String password;
    public @OneToMany(mappedBy = "user") ArrayList<RatedPoem> readPoems = new ArrayList<RatedPoem>();

    public User(){

    }

    public User(String userName, String password){
        this.userName = userName;
        this.password = password;
    }

}
