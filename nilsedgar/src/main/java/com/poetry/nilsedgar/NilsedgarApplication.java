package com.poetry.nilsedgar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NilsedgarApplication {

	public static void main(String[] args) {
		SpringApplication.run(NilsedgarApplication.class, args);
	}

}
