package com.poetry.nilsedgar.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.poetry.nilsedgar.model.User;

@RepositoryRestResource
public interface UserRepository extends CrudRepository<User, Long> {
    
}
