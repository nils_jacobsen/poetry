package com.poetry.nilsedgar.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.poetry.nilsedgar.model.Poem;

@RepositoryRestResource
public interface PoemRepository extends CrudRepository<Poem, Long> {
    
}
