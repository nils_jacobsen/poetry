package com.poetry.nilsedgar.controller;

import java.util.Collection;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.poetry.nilsedgar.model.Poem;
import com.poetry.nilsedgar.repository.PoemRepository;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:3000")
public class PoemController {

    private PoemRepository poemRepository;

    public PoemController(PoemRepository poemRepository){
        this.poemRepository = poemRepository;
    }

    @GetMapping("/poems")
    public Collection<Poem> getPoems(){
        return (Collection<Poem>) poemRepository.findAll();
    }

    @PostMapping("/poems/free-form")
    public ResponseEntity<Poem> writeFreeFormPoem(@RequestBody Poem poem){
        poem.form = "free";
        return ResponseEntity
                .ok(poemRepository.save(poem));
    }

    @PostMapping("/poems/haiku")
    public ResponseEntity<Poem> writeHaiku(@RequestBody Poem poem){
        poem.form = "haiku";
        return ResponseEntity
                .ok(poemRepository.save(poem));
    }
    
}
