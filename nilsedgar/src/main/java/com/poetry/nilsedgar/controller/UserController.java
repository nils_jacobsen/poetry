package com.poetry.nilsedgar.controller;

import java.util.Collection;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.poetry.nilsedgar.model.Poem;
import com.poetry.nilsedgar.model.RatedPoem;
import com.poetry.nilsedgar.model.User;
import com.poetry.nilsedgar.repository.PoemRepository;
import com.poetry.nilsedgar.repository.UserRepository;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:3000")
public class UserController {

    private PoemRepository poemRepository;
    private UserRepository userRepository;

    public UserController(PoemRepository poemRepository, UserRepository userRepository){
        this.poemRepository = poemRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("/users")
    public Collection<User> getUsers(){
        return (Collection<User>) userRepository.findAll();
    }

    @PostMapping("/{user_id}/ratePoem")
    public ResponseEntity<User> ratePoem(@PathVariable Long user_id, @RequestBody RatedPoem ratedPoem){
        User user = userRepository.findById(user_id).get();
        ratedPoem.setRating(ratedPoem.rating);
        return ResponseEntity
                .ok(userRepository.save(user));
    }
    
}

